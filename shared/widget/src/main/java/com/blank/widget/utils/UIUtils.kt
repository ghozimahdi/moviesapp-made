package com.blank.widget.utils

import android.content.Context
import android.graphics.Bitmap
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlin.math.roundToInt

/**
 * Created by knalb on 10/06/19.
 * Email : profghozimahdi@gmail.com
 * No Tpln : 0857-4124-4919
 * Profesi : Mobile Development Engineer
 */

fun View.hide() {
    visibility = View.GONE
}

fun View.show() {
    visibility = View.VISIBLE
}

fun getRoundedImageTarget(
    context: Context,
    imageView: ImageView,
    radius: Float
): BitmapImageViewTarget =
    object : BitmapImageViewTarget(imageView) {
        override fun setResource(resource: Bitmap?) {
            val circularBitmapDrawable =
                RoundedBitmapDrawableFactory.create(context.resources, resource)
            circularBitmapDrawable.cornerRadius = radius
            imageView.setImageDrawable(circularBitmapDrawable)
        }
    }

fun TabLayout.setupWithViewPager(viewPager2: ViewPager2, labels: Array<String>) {
    TabLayoutMediator(this, viewPager2) { tab, position ->
        tab.text = labels[position]
    }.attach()
}

fun <T> LifecycleOwner.observe(liveData: LiveData<T>, action: (T) -> Unit) {
    liveData.observe(this, Observer(action))
}

fun AppCompatImageView.setImageRounded(url: String, radius: Float) {
    val requestOptions = RequestOptions().optionalTransform(RoundedCorners(9))

    Glide.with(this)
        .asBitmap()
        .load(url)
        .apply(requestOptions)
        .into(getRoundedImageTarget(context, this, radius))
}

fun ImageView.setImages(url: String) {
    Glide.with(this)
        .load(url)
        .into(this)
}

infix fun Context.dpToPx(dp: Int): Int {
    val r = resources
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r?.displayMetrics)
        .roundToInt()
}