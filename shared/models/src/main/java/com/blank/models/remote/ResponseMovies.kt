package com.blank.models.remote

import androidx.room.TypeConverter
import com.blank.models.local.Genres
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


const val ENDPOINT_IMAGE_ORIGIN = "https://image.tmdb.org/t/p/original"
const val ENDPOINT_IMAGE_CUSTOM = "https://image.tmdb.org/t/p/w500"
const val NO_IMAGE_FOUND = "https://www.jlnmcbgp.org/images/no_photo_available.png"

data class ResponseMovies(

    @field:SerializedName("dates")
    val dates: Dates? = null,

    @field:SerializedName("page")
    val page: Int? = null,

    @field:SerializedName("total_pages")
    val totalPages: Int? = null,

    @field:SerializedName("results")
    val results: List<ResultsItem> = listOf(),

    @field:SerializedName("total_results")
    val totalResults: Int? = null
)

data class ResultsItem(

    @field:SerializedName("overview")
    val overview: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("genre_ids")
    val genreIds: List<Int> = listOf(),

    var genres: MutableList<GenresItem> = mutableListOf(),

    @field:SerializedName("poster_path")
    val posterPath: String? = null,

    @field:SerializedName("backdrop_path")
    val backdropPath: String? = null,

    @field:SerializedName("release_date")
    val releaseDate: String = "",

    @field:SerializedName("first_air_date")
    val firstAirDate: String = "",

    @field:SerializedName("id")
    val id: Int? = null,
)

data class Dates(

    @field:SerializedName("maximum")
    val maximum: String? = null,

    @field:SerializedName("minimum")
    val minimum: String? = null
)


object Converters {
    @TypeConverter
    @JvmStatic
    fun fromString(value: String): List<Int> {
        val listType: Type = object : TypeToken<List<Int>>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    @JvmStatic
    fun fromArrayList(list: List<Int>): String = Gson().toJson(list)

    @TypeConverter
    @JvmStatic
    fun fromStringGenres(value: String): List<Genres> {
        val listType: Type = object : TypeToken<List<Genres>>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    @JvmStatic
    fun fromListGenres(list: List<Genres>): String = Gson().toJson(list)
}