package com.blank.models.remote

import com.google.gson.annotations.SerializedName

data class MsgGenres(

	@field:SerializedName("genres")
	val genres: List<GenresItem> = listOf()
)

data class GenresItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
