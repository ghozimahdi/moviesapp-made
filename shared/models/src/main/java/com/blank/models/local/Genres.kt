package com.blank.models.local

import android.os.Parcelable
import com.blank.models.remote.GenresItem
import kotlinx.parcelize.Parcelize

@Parcelize
data class Genres(
    val name: String? = null,
    val id: Int? = null
) : Parcelable {
    companion object {
        fun mappingGenresItems(genresItem: List<GenresItem>) =
            genresItem.map {
                Genres(
                    name = it.name,
                    id = it.id
                )
            }

        fun mappingGenres(genresItem: List<Genres>) =
            genresItem.map {
                Genres(
                    name = it.name,
                    id = it.id
                )
            }
    }
}
