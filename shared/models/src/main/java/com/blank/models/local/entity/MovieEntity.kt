package com.blank.models.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.blank.models.local.Genres
import com.blank.models.local.Movies

@Entity(tableName = "Movies")
data class MovieEntity(

    @ColumnInfo(name = "overview")
    val overview: String? = null,

    @ColumnInfo(name = "title")
    val title: String? = null,

    @ColumnInfo(name = "name")
    val name: String? = null,

    @ColumnInfo(name = "genre_ids")
    val genreIds: List<Int> = listOf(),

    @ColumnInfo(name = "genres_item")
    var genres: List<Genres> = listOf(),

    @ColumnInfo(name = "poster_path")
    val posterPath: String? = null,

    @ColumnInfo(name = "backdrop_path")
    val backdropPath: String? = null,

    @ColumnInfo(name = "release_date")
    val releaseDate: String = "",

    @ColumnInfo(name = "first_air_date")
    val firstAirDate: String = "",

    @PrimaryKey
    val id: Int? = null,

    @ColumnInfo(name = "type")
    var type: Int = 0
) {
    companion object {
        fun mappingMoviesEntity(movies: Movies) =
            MovieEntity(
                overview = movies.overview,
                title = movies.title,
                name = movies.name,
                genreIds = movies.genreIds,
                genres = movies.genres,
                posterPath = movies.posterPath,
                backdropPath = movies.backdropPath,
                releaseDate = movies.releaseDate,
                firstAirDate = movies.firstAirDate,
                id = movies.id,
                type = movies.type
            )
    }
}