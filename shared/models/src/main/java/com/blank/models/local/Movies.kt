package com.blank.models.local

import android.os.Parcelable
import com.blank.models.local.Genres.Companion.mappingGenres
import com.blank.models.local.Genres.Companion.mappingGenresItems
import com.blank.models.local.entity.MovieEntity
import com.blank.models.remote.ENDPOINT_IMAGE_CUSTOM
import com.blank.models.remote.ENDPOINT_IMAGE_ORIGIN
import com.blank.models.remote.NO_IMAGE_FOUND
import com.blank.models.remote.ResponseMovies
import kotlinx.parcelize.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class Movies(
    val overview: String? = null,
    val title: String? = null,
    val name: String? = null,
    val genreIds: List<Int> = listOf(),
    var genres: List<Genres> = listOf(),
    val posterPath: String? = null,
    val backdropPath: String? = null,
    val releaseDate: String = "",
    val firstAirDate: String = "",
    val id: Int? = null,
    var type: Int = 0
) : Parcelable {
    fun getPoster(): String =
        if (posterPath == null) NO_IMAGE_FOUND else ENDPOINT_IMAGE_CUSTOM.plus(posterPath)

    fun getPosterBg(): String =
        if (backdropPath == null) NO_IMAGE_FOUND else ENDPOINT_IMAGE_ORIGIN.plus(backdropPath)

    fun getDate(): String {
        val parser = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val formatter = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
        return if (type == 0) {
            if (releaseDate != "")
                formatter.format(parser.parse(releaseDate))
            else
                "-"
        } else {
            if (firstAirDate != "")
                formatter.format(parser.parse(firstAirDate))
            else
                "-"
        }
    }

    fun allGenres(): String {
        var text = ""
        genres.forEachIndexed { index, genresItem ->
            text = text.plus(genresItem.name)
            if (index != genres.size.minus(1))
                text = text.plus(", ")
        }
        return text
    }

    companion object {
        fun mappingMovies(moviesEntity: List<MovieEntity>) = moviesEntity.map {
            Movies(
                overview = it.overview,
                title = it.title,
                name = it.name,
                genreIds = it.genreIds,
                genres = mappingGenres(it.genres),
                posterPath = it.posterPath,
                backdropPath = it.backdropPath,
                releaseDate = it.releaseDate,
                firstAirDate = it.firstAirDate,
                id = it.id,
                type = it.type
            )
        }

        fun mappingMoviesItems(responseMovies: ResponseMovies): List<Movies> =
            responseMovies.results.map {
                Movies(
                    overview = it.overview,
                    title = it.title,
                    name = it.name,
                    genreIds = it.genreIds,
                    genres = mappingGenresItems(it.genres),
                    posterPath = it.posterPath,
                    backdropPath = it.backdropPath,
                    releaseDate = it.releaseDate,
                    firstAirDate = it.firstAirDate,
                    id = it.id
                )
            }
    }
}