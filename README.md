# Movies-AndroidApp
Movies Android Application

This project using the Architecture Components:
- Room
- Lifecycle-aware components
- ViewModels
- LiveData
- Paging3
- Navigation
- DataBinding
- WorkManager

## Screenshots

<p align="center">
  <img src="ss/movies.png" width="270" alt="Movies">
  <img src="ss/detail.png" width="270" alt="Detail">
</p>

## Development Roadmap

- [x] [Kotlin](https://kotlinlang.org/)
- [x] [Room](https://developer.android.com/topic/libraries/architecture/room)
- [x] [Lifecycle-aware components](https://developer.android.com/topic/libraries/architecture/lifecycle)
- [x] [ViewModels](https://developer.android.com/topic/libraries/architecture/viewmodel)
- [x] [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
- [x] [Paging3](https://developer.android.com/topic/libraries/architecture/paging/)
- [x] [Navigation](https://developer.android.com/topic/libraries/architecture/navigation)
- [x] [DataBinding](https://developer.android.com/topic/libraries/data-binding)
- [ ] [WorkManager](https://developer.android.com/topic/libraries/architecture/workmanager/)
- [ ] [AlarmManager](https://developer.android.com/training/scheduling/alarms)
- [ ] [Coroutines](https://developer.android.com/topic/libraries/architecture/coroutines)
- [ ] [Gradle Kotlin DSL](https://docs.gradle.org/current/userguide/kotlin_dsl.html)
- [x] [Retrofit](https://square.github.io/retrofit/)
- [ ] [Koin](https://insert-koin.io/)
- [ ] [Ktlint](https://ktlint.github.io/)
- [x] [RxAndroid](https://medium.com/easyread/mencoba-implementasi-rxjava-pada-aplikasi-android-f606a195b7f1)
- [x] [RxJava](https://medium.com/easyread/mencoba-implementasi-rxjava-pada-aplikasi-android-f606a195b7f1)
- [x] [Dagger Hilt](https://developer.android.com/training/dependency-injection/hilt-android)
- [x] [Android KTX](https://developer.android.com/kotlin/ktx?gclid=CjwKCAiA17P9BRB2EiwAMvwNyA27ZQGKeGr-_YoHGLwFwjW8lmMjLP33i3o-F0F_tP_kc5oqeuM04BoCNT4QAvD_BwE&gclsrc=aw.ds)
- [x] JUnit
- [x] Mockito
- [x] [Spek](https://www.spekframework.org/setup-android/)
- [ ] [MotionLayout](https://developer.android.com/training/constraint-layout/motionlayout)
- [x] Transition Animations
- [x] [MODULARIZATION](https://github.com/BestPractice-AndroidDev/android-modular-architecture-example)
- [x] [CI/CD](https://medium.com/kode-dan-kodean/menggunakan-gitlab-ci-untuk-build-android-apk-405217cf525d)

## Features

- [x] Movies List
- [x] TV Movies List
- [x] Detail Movies
- [x] Search Movies
- [x] VIEW ALL Movies
- [x] Movies Favorite List
- [ ] About

## License

All the code available under the Apache License. See [LICENSE](LICENSE).

```
Apache License

Copyright (c) 2020 Ghozi Mahdi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
