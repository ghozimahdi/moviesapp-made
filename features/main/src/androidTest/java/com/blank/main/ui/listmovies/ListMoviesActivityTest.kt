package com.blank.main.ui.listmovies

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.blank.core.utils.KEY_REQ_MOVIES
import com.blank.core.utils.instrumenttest.EspressoIdlingResource
import com.blank.main.R
import com.blank.main.ui.utils.ClickOnImageViewFavorite
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class ListMoviesActivityTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Before
    fun setUp() {
        hiltRule.inject()
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getEspressoIdlingResource())
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getEspressoIdlingResource())
    }

    private fun launchListMovies() {
        val targetContext: Context = InstrumentationRegistry.getInstrumentation().targetContext
        val intent = Intent(targetContext, ListMoviesActivity::class.java)
        intent.putExtra(KEY_REQ_MOVIES, 0)
        ActivityScenario.launch<ListMoviesActivity>(intent)
    }

    @Test
    fun loadViewAllMovies() {
        launchListMovies()
        Espresso.onView(withId(R.id.rvListMovies)).check(matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun gotoDetail() {
        launchListMovies()

        Espresso.onView(withId(R.id.rvListMovies)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                ClickOnImageViewFavorite()
            )
        )

        try {
            Thread.sleep(5000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }
}