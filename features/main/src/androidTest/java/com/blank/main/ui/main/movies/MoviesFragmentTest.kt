package com.blank.main.ui.main.movies

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.blank.core.utils.instrumenttest.EspressoIdlingResource
import com.blank.main.R
import com.blank.main.ui.utils.ClickOnImageView
import com.blank.main.ui.utils.launchActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class MoviesFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Before
    fun setUp() {
        hiltRule.inject()
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getEspressoIdlingResource())
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getEspressoIdlingResource())
    }

    @Test
    fun loadMovies() {
        launchActivity()

        onView(withId(R.id.rvPopular)).check(matches(isDisplayed()))
    }

    @Test
    fun clickMovies() {
        launchActivity()

        onView(withId(R.id.rvPopular)).check(matches(isDisplayed()))
        onView(withId(R.id.rvPopular)).perform(
            actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                ClickOnImageView()
            )
        )

        try {
            Thread.sleep(5000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }
}