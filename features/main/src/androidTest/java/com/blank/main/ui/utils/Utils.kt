package com.blank.main.ui.utils

import android.view.View
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions
import com.blank.main.R
import com.blank.main.ui.main.MainActivity
import org.hamcrest.Matcher

class ClickOnImageView : ViewAction {
    private val click = ViewActions.click()
    override fun getConstraints(): Matcher<View> {
        return click.constraints
    }

    override fun getDescription(): String {
        return " click on custom image view"
    }

    override fun perform(uiController: UiController?, view: View) {
        click.perform(uiController, view.findViewById(R.id.itemImg))
    }
}

class ClickOnImageViewFavorite : ViewAction {
    private val click = ViewActions.click()
    override fun getConstraints(): Matcher<View> {
        return click.constraints
    }

    override fun getDescription(): String {
        return " click on custom image view"
    }

    override fun perform(uiController: UiController?, view: View) {
        click.perform(uiController, view.findViewById(R.id.iCListMovies))
    }
}

fun launchActivity(): ActivityScenario<MainActivity> =
    ActivityScenario.launch(MainActivity::class.java)