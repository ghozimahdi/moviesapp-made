package com.blank.main.ui.main.tvmovies

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.blank.core.utils.instrumenttest.EspressoIdlingResource
import com.blank.main.R
import com.blank.main.ui.utils.ClickOnImageView
import com.blank.main.ui.utils.launchActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class TVMoviesFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    private lateinit var bottomNavigationView: BottomNavigationView

    @Before
    fun setUp() {
        hiltRule.inject()
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getEspressoIdlingResource())
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getEspressoIdlingResource())
    }

    @Test
    fun loadMovies() {
        launchActivity().onActivity {
            bottomNavigationView = it.findViewById(R.id.bottomNavView)
            bottomNavigationView.selectedItemId = R.id.tvMoviesFragment
        }

        Espresso.onView(withId(R.id.rvTvPopular))
            .check(ViewAssertions.matches(isDisplayed()))

        try {
            Thread.sleep(2000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    @Test
    fun clickMovies() {
        launchActivity().onActivity {
            bottomNavigationView = it.findViewById(R.id.bottomNavView)
            bottomNavigationView.selectedItemId = R.id.tvMoviesFragment
        }

        Espresso.onView(withId(R.id.rvTvPopular)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                1,
                ClickOnImageView()
            )
        )

        try {
            Thread.sleep(5000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }
}