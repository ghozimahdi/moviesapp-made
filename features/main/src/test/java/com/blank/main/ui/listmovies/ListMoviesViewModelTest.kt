package com.blank.main.ui.listmovies

import androidx.lifecycle.Observer
import androidx.paging.PagingData
import com.blank.main.helper.InstantRuleExecution
import com.blank.main.helper.TrampolineSchedulerRX
import com.blank.main.ui.listmovies.source.ListMoviesSource
import com.blank.models.local.Movies
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Flowable
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

@RunWith(JUnitPlatform::class)
class ListMoviesViewModelTest : Spek({
    Feature("ListMovies") {
        val source = mock<ListMoviesSource>()
        val viewModel = ListMoviesViewModel(source)
        val observerMovies = mock<Observer<PagingData<Movies>>>()

        beforeFeature {
            InstantRuleExecution.start()
            TrampolineSchedulerRX.start()
            viewModel.resultItems.observeForever(observerMovies)
        }

        Scenario("do on get list movies, and success response") {
            val expectedResult = PagingData.from(
                mutableListOf(
                    Movies(title = "test", id = 1)
                )
            )

            Given("set success") {
                given(source.getPager(0)).willReturn(
                    Flowable.just(expectedResult)
                )
            }

            When("call get movies and set req = 0") {
                viewModel.getMovies(0)
            }

            Then("result success and return data") {
                verify(observerMovies, atLeastOnce()).onChanged(expectedResult)
            }
        }

        afterFeature {
            InstantRuleExecution.tearDown()
            TrampolineSchedulerRX.tearDown()
        }
    }
})