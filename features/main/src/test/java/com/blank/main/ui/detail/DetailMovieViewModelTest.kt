package com.blank.main.ui.detail

import androidx.lifecycle.Observer
import com.blank.core.data.DataManager
import com.blank.main.helper.InstantRuleExecution
import com.blank.main.helper.TrampolineSchedulerRX
import com.blank.models.local.entity.MovieEntity
import com.blank.models.local.Movies
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature


@RunWith(JUnitPlatform::class)
class DetailMovieViewModelTest : Spek({
    Feature("Detail Movies") {
        val appDataManager = mock<DataManager>()
        val viewModel = DetailMovieViewModel(appDataManager)
        val observerMovies = mock<Observer<Movies>>()
        val observerIsLoading = mock<Observer<Boolean>>()
        val observerSaveResultState = mock<Observer<Boolean>>()
        val observerIsSave = mock<Observer<Boolean>>()

        beforeFeature {
            InstantRuleExecution.start()
            TrampolineSchedulerRX.start()
            viewModel.movies.observeForever(observerMovies)
            viewModel.isLoading.observeForever(observerIsLoading)
            viewModel.saveResultState.observeForever(observerSaveResultState)
            viewModel.isSave.observeForever(observerIsSave)
        }

        Scenario("do on set movies, and success response") {
            Given("set fetch Movies ById success") {
                given(appDataManager.fetchMoviesById(1)).willReturn(Single.just(true))
            }

            When("set movies") {
                viewModel.setMovies(Movies(title = "test", id = 1))
            }

            Then("result success and return data") {
                verify(observerIsLoading, atLeastOnce()).onChanged(true)
                verify(observerIsSave, atLeastOnce()).onChanged(true)
            }
        }

        Scenario("do on set movies, and success response") {
            Given("set fetch Movies ById failed") {
                given(appDataManager.fetchMoviesById(1)).willReturn(Single.just(false))
            }

            When("set movies") {
                viewModel.setMovies(Movies(title = "test", id = 1))
            }

            Then("result success and return data") {
                verify(observerIsLoading, atLeastOnce()).onChanged(true)
                verify(observerIsSave, atLeastOnce()).onChanged(false)
            }
        }

        Scenario("do on save movies to favorite, and success response") {
            val expectedResult = true

            Given("set success save movies") {
                given(appDataManager.insertMovie(MovieEntity())).willReturn(
                    Observable.just(
                        expectedResult
                    )
                )
            }

            When("save movies") {
                viewModel.saveToFavorite(true, Movies())
            }

            Then("result success and return data") {
                verify(observerSaveResultState, atLeastOnce()).onChanged(true)
            }
        }

        Scenario("do on save movies to favorite, and error response") {
            val expectedResult = false

            Given("set error save movies") {
                given(appDataManager.deleteMovie(MovieEntity())).willReturn(
                    Observable.just(
                        expectedResult
                    )
                )
            }

            When("save movies") {
                viewModel.saveToFavorite(false, Movies())
            }

            Then("result success and return data") {
                verify(observerSaveResultState, atLeastOnce()).onChanged(false)
            }
        }

        afterFeature {
            InstantRuleExecution.tearDown()
            TrampolineSchedulerRX.tearDown()
        }
    }
})