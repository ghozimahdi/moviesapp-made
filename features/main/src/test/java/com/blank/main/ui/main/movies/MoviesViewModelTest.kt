package com.blank.main.ui.main.movies

import androidx.lifecycle.Observer
import com.blank.core.data.AppDataManager
import com.blank.core.utils.api.ResultState
import com.blank.main.helper.InstantRuleExecution
import com.blank.main.helper.TrampolineSchedulerRX
import com.blank.models.remote.ResultsItem
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

@RunWith(JUnitPlatform::class)
class MoviesViewModelTest : Spek({
    Feature("Movies") {
        val appDataManager = mock<AppDataManager>()
        val viewModel = MoviesViewModel(appDataManager)
        val observerPopularMovies = mock<Observer<ResultState>>()
        val observerTopratedMovies = mock<Observer<ResultState>>()
        val observerUpcomingMovies = mock<Observer<ResultState>>()

        beforeFeature {
            TrampolineSchedulerRX.start()
            InstantRuleExecution.start()
            viewModel.popularMoviesResult.observeForever(observerPopularMovies)
            viewModel.toprateMoviesResult.observeForever(observerTopratedMovies)
            viewModel.upcomingMoviesResult.observeForever(observerUpcomingMovies)
        }

        Scenario("do on get popular movies, success result") {
            val expectedResult = listOf<ResultsItem>()
            var expectedResultState: ResultState

            Given("set success result state") {
                expectedResultState = ResultState.Success(expectedResult)

                given(appDataManager.getPopularMovies()).willReturn(Single.just(expectedResultState))
            }

            When("get popular movies") {
                viewModel.getPopularMovies()
            }

            Then("result success and return data") {
                verify(observerPopularMovies, atLeastOnce()).onChanged(ResultState.Loading(true))
                verify(observerPopularMovies, atLeastOnce()).onChanged(ResultState.Loading(false))
                verify(observerPopularMovies, atLeastOnce()).onChanged(
                    ResultState.Success(
                        expectedResult
                    )
                )
            }
        }

        Scenario("do on get popular movies, error result") {
            val expectedResult = Throwable("Error")

            Given("set error result state") {
                given(appDataManager.getPopularMovies()).willReturn(Single.error(expectedResult))
            }

            When("get popular movies") {
                viewModel.getPopularMovies()
            }

            Then("result error and return data") {
                verify(observerPopularMovies, atLeastOnce()).onChanged(ResultState.Loading(true))
                verify(observerPopularMovies, atLeastOnce()).onChanged(ResultState.Loading(false))
                verify(observerPopularMovies, atLeastOnce()).onChanged(
                    ResultState.Error(
                        expectedResult
                    )
                )
            }
        }

        Scenario("do on get toprated movies, success result") {
            val expectedResult = listOf<ResultsItem>()
            var expectedResultState: ResultState

            Given("set success result state") {
                expectedResultState = ResultState.Success(expectedResult)

                given(appDataManager.getTopRateMovies()).willReturn(Single.just(expectedResultState))
            }

            When("get toprated movies") {
                viewModel.getTopRateMovies()
            }

            Then("result success and return data") {
                verify(observerTopratedMovies, atLeastOnce()).onChanged(ResultState.Loading(true))
                verify(observerTopratedMovies, atLeastOnce()).onChanged(ResultState.Loading(false))
                verify(observerTopratedMovies, atLeastOnce()).onChanged(
                    ResultState.Success(
                        expectedResult
                    )
                )
            }
        }

        Scenario("do on get toprated movies, error result") {
            val expectedResult = Throwable("Error")

            Given("set error result state") {
                given(appDataManager.getTopRateMovies()).willReturn(Single.error(expectedResult))
            }

            When("get toprated movies") {
                viewModel.getTopRateMovies()
            }

            Then("result error and return data") {
                verify(observerTopratedMovies, atLeastOnce()).onChanged(ResultState.Loading(true))
                verify(observerTopratedMovies, atLeastOnce()).onChanged(ResultState.Loading(false))
                verify(observerTopratedMovies, atLeastOnce()).onChanged(
                    ResultState.Error(
                        expectedResult
                    )
                )
            }
        }

        Scenario("do on get upcoming movies, success result") {
            val expectedResult = listOf<ResultsItem>()
            var expectedResultState: ResultState

            Given("set success result state") {
                expectedResultState = ResultState.Success(expectedResult)

                given(appDataManager.getUpComingMovies()).willReturn(Single.just(expectedResultState))
            }

            When("get upcoming movies") {
                viewModel.getUpComingMovies()
            }

            Then("result success and return data") {
                verify(observerUpcomingMovies, atLeastOnce()).onChanged(ResultState.Loading(true))
                verify(observerUpcomingMovies, atLeastOnce()).onChanged(ResultState.Loading(false))
                verify(observerUpcomingMovies, atLeastOnce()).onChanged(
                    ResultState.Success(
                        expectedResult
                    )
                )
            }
        }

        Scenario("do on get upcoming movies, error result") {
            val expectedResult = Throwable("Error")

            Given("set success result state") {
                given(appDataManager.getUpComingMovies()).willReturn(Single.error(expectedResult))
            }

            When("get upcoming movies") {
                viewModel.getUpComingMovies()
            }

            Then("result error and return data") {
                verify(observerUpcomingMovies, atLeastOnce()).onChanged(ResultState.Loading(true))
                verify(observerUpcomingMovies, atLeastOnce()).onChanged(ResultState.Loading(false))
                verify(observerUpcomingMovies, atLeastOnce()).onChanged(
                    ResultState.Error(
                        expectedResult
                    )
                )
            }
        }

        afterFeature {
            TrampolineSchedulerRX.tearDown()
            InstantRuleExecution.tearDown()
        }
    }
})