package com.blank.main.ui.main.tvmovies

import androidx.lifecycle.Observer
import com.blank.core.data.AppDataManager
import com.blank.core.utils.api.ResultState
import com.blank.main.helper.InstantRuleExecution
import com.blank.main.helper.TrampolineSchedulerRX
import com.blank.models.remote.ResultsItem
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

@RunWith(JUnitPlatform::class)
class TVMoviesViewModelTest : Spek({
    Feature("TV Movies") {
        val appDataManager = mock<AppDataManager>()
        val viewModel = TVMoviesViewModel(appDataManager)
        val observerTvMoviesPopular = mock<Observer<ResultState>>()
        val observerTvMoviesToprated = mock<Observer<ResultState>>()

        beforeFeature {
            TrampolineSchedulerRX.start()
            InstantRuleExecution.start()
            viewModel.tvmoviesPopularResultState.observeForever(observerTvMoviesPopular)
            viewModel.tvmoviesTopRatedResultState.observeForever(observerTvMoviesToprated)
        }

        Scenario("do on get data TV Movies Popular, response success") {
            val expectedResult = listOf<ResultsItem>()
            var expectedResultState: ResultState

            Given("set success result state") {
                expectedResultState = ResultState.Success(expectedResult)

                given(appDataManager.getPopularTVMovies()).willReturn(
                    Single.just(
                        expectedResultState
                    )
                )
            }

            When("get TV Movies Popular") {
                viewModel.getTvMoviesPopular()
            }

            Then("result success and return data") {
                verify(observerTvMoviesPopular, atLeastOnce()).onChanged(ResultState.Loading(true))
                verify(
                    observerTvMoviesPopular,
                    atLeastOnce()
                ).onChanged(ResultState.Success(expectedResult))
                verify(observerTvMoviesPopular, atLeastOnce()).onChanged(ResultState.Loading(false))
            }
        }

        Scenario("do on get data TV Movies Popular, response error") {
            val expectedResult = Throwable("Error")
            var expectedResultState: ResultState

            Given("set error result state") {
                expectedResultState = ResultState.Error(expectedResult)

                given(appDataManager.getPopularTVMovies()).willReturn(
                    Single.just(expectedResultState)
                )
            }

            When("get TV Movies Popular") {
                viewModel.getTvMoviesPopular()
            }

            Then("result error") {
                verify(observerTvMoviesPopular, atLeastOnce()).onChanged(ResultState.Loading(true))
                verify(
                    observerTvMoviesPopular,
                    atLeastOnce()
                ).onChanged(ResultState.Error(expectedResult))
                verify(observerTvMoviesPopular, atLeastOnce()).onChanged(ResultState.Loading(false))
            }
        }

        Scenario("do on get data TV Movies Toprated, response success") {
            val expectedResult = listOf<ResultsItem>()
            var expectedResultState: ResultState

            Given("set success result state") {
                expectedResultState = ResultState.Success(expectedResult)

                given(appDataManager.getTopratedTVMovies()).willReturn(
                    Single.just(
                        expectedResultState
                    )
                )
            }

            When("get TV Movies Toprated") {
                viewModel.getTvMoviesToprated()
            }

            Then("result success and return data") {
                verify(observerTvMoviesToprated, atLeastOnce()).onChanged(ResultState.Loading(true))
                verify(
                    observerTvMoviesToprated,
                    atLeastOnce()
                ).onChanged(ResultState.Success(expectedResult))
                verify(
                    observerTvMoviesToprated,
                    atLeastOnce()
                ).onChanged(ResultState.Loading(false))
            }
        }

        Scenario("do on get data TV Movies Toprated, response error") {
            val expectedResult = Throwable("Error")
            var expectedResultState: ResultState

            Given("set error result state") {
                expectedResultState = ResultState.Error(expectedResult)

                given(appDataManager.getTopratedTVMovies()).willReturn(
                    Single.just(expectedResultState)
                )
            }

            When("get TV Movies Toprated") {
                viewModel.getTvMoviesToprated()
            }

            Then("result error") {
                verify(observerTvMoviesToprated, atLeastOnce()).onChanged(ResultState.Loading(true))
                verify(
                    observerTvMoviesToprated,
                    atLeastOnce()
                ).onChanged(ResultState.Error(expectedResult))
                verify(
                    observerTvMoviesToprated,
                    atLeastOnce()
                ).onChanged(ResultState.Loading(false))
            }
        }

        afterFeature {
            TrampolineSchedulerRX.tearDown()
            InstantRuleExecution.tearDown()
        }
    }
})