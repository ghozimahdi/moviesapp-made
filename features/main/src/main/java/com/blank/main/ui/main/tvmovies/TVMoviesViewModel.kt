package com.blank.main.ui.main.tvmovies

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.blank.core.base.BaseViewModel
import com.blank.core.data.DataManager
import com.blank.core.utils.api.ResultState
import com.blank.core.utils.instrumenttest.EspressoIdlingResource

class TVMoviesViewModel @ViewModelInject constructor(private val dataManager: DataManager) :
    BaseViewModel() {

    val tvmoviesPopularResultState = MutableLiveData<ResultState>()
    val tvmoviesTopRatedResultState = MutableLiveData<ResultState>()

    fun getTvMoviesPopular() {
        EspressoIdlingResource.increment()
        dataManager.getPopularTVMovies()
            .doAfterTerminate {
                EspressoIdlingResource.decrement()
                tvmoviesPopularResultState.value = ResultState.Loading(false)
            }
            .doOnSubscribe {
                tvmoviesPopularResultState.value = ResultState.Loading(true)
            }
            .subscribe({
                tvmoviesPopularResultState.value = it
            }, {
                tvmoviesPopularResultState.value = ResultState.Error(it)
            }).autoDispose()
    }

    fun getTvMoviesToprated() {
        EspressoIdlingResource.increment()
        dataManager.getTopratedTVMovies()
            .doAfterTerminate {
                EspressoIdlingResource.decrement()
                tvmoviesTopRatedResultState.value = ResultState.Loading(false)
            }
            .doOnSubscribe {
                tvmoviesTopRatedResultState.value = ResultState.Loading(true)
            }
            .subscribe({
                tvmoviesTopRatedResultState.value = it
            }, {
                tvmoviesTopRatedResultState.value = ResultState.Error(it)
            }).autoDispose()
    }
}