package com.blank.main.ui.main.tvmovies

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.blank.core.base.BaseFragment
import com.blank.core.navigation.Navigator
import com.blank.core.utils.api.ResultState
import com.blank.core.utils.view.HorizontalSpaceItemDecoration
import com.blank.core.utils.view.PeekingLinearLayoutManager
import com.blank.main.BR
import com.blank.main.R
import com.blank.main.databinding.FragmentTVMoviesBinding
import com.blank.main.ui.main.tvmovies.adapter.PopularTVMoviesAdapter
import com.blank.main.ui.main.tvmovies.adapter.TopratedTVMoviesAdapter
import com.blank.models.local.Movies
import com.blank.widget.utils.dpToPx
import com.blank.widget.utils.observe
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class TVMoviesFragment : BaseFragment<FragmentTVMoviesBinding, TVMoviesViewModel>() {

    override val viewModel by viewModels<TVMoviesViewModel>()
    override fun layoutId() = R.layout.fragment_t_v_movies
    override fun bindingVariable() = BR.vmtvmovies

    @Inject
    lateinit var popularAdapter: PopularTVMoviesAdapter

    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var topratedTVMoviesAdapter: TopratedTVMoviesAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.rvTvPopular?.apply {
            layoutManager =
                PeekingLinearLayoutManager(
                    requireContext(),
                    0.38f,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            addItemDecoration(HorizontalSpaceItemDecoration(requireContext() dpToPx 8))
            adapter = popularAdapter
        }
        popularAdapter.setListener(navigator::goToDetail)

        binding?.rvTvTopRate?.apply {
            layoutManager =
                PeekingLinearLayoutManager(
                    requireContext(),
                    0.38f,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            addItemDecoration(HorizontalSpaceItemDecoration(requireContext() dpToPx 8))
            adapter = topratedTVMoviesAdapter
        }
        topratedTVMoviesAdapter.setListener(navigator::goToDetail)

        observe(viewModel.tvmoviesPopularResultState, this::manageTvMoviesPopularResultState)
        observe(viewModel.tvmoviesTopRatedResultState, this::manageTvMoviesTopratedResultState)
        viewModel.getTvMoviesPopular()
        viewModel.getTvMoviesToprated()
    }

    private fun manageTvMoviesPopularResultState(state: ResultState) {
        when (state) {
            is ResultState.Error -> {
                state.e.printStackTrace()
                val msg = state.e.message.toString()
                Timber.e(msg)
                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            }
            is ResultState.Loading -> {
                binding?.pbtvMovies?.isVisible = state.isLoading
            }
            is ResultState.Success<*> -> {
                popularAdapter.setData(state.data as MutableList<Movies>)
            }
        }
    }

    private fun manageTvMoviesTopratedResultState(state: ResultState) {
        when (state) {
            is ResultState.Error -> {
                state.e.printStackTrace()
                val msg = state.e.message.toString()
                Timber.e(msg)
                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            }
            is ResultState.Loading -> {
                binding?.pbtvToprated?.isVisible = state.isLoading
            }
            is ResultState.Success<*> -> {
                topratedTVMoviesAdapter.setData(state.data as MutableList<Movies>)
            }
        }
    }
}