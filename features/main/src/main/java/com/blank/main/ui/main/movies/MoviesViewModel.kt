package com.blank.main.ui.main.movies

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.blank.core.base.BaseViewModel
import com.blank.core.data.DataManager
import com.blank.core.utils.api.ResultState
import com.blank.core.utils.instrumenttest.EspressoIdlingResource

class MoviesViewModel @ViewModelInject constructor(
    private val dataManager: DataManager
) : BaseViewModel() {

    val popularMoviesResult = MutableLiveData<ResultState>()
    val upcomingMoviesResult = MutableLiveData<ResultState>()
    val toprateMoviesResult = MutableLiveData<ResultState>()

    fun getPopularMovies() {
        EspressoIdlingResource.increment()
        dataManager.getPopularMovies()
            .doOnSubscribe {
                popularMoviesResult.value = ResultState.Loading(true)
            }
            .doAfterTerminate {
                EspressoIdlingResource.decrement()
                popularMoviesResult.value = ResultState.Loading(false)
            }
            .subscribe({
                popularMoviesResult.value = it
            }, {
                popularMoviesResult.value = ResultState.Error(it)
            })
            .autoDispose()
    }

    fun getUpComingMovies() {
        EspressoIdlingResource.increment()
        dataManager.getUpComingMovies()
            .doOnSubscribe {
                upcomingMoviesResult.value = ResultState.Loading(true)
            }
            .doAfterTerminate {
                EspressoIdlingResource.decrement()
                upcomingMoviesResult.value = ResultState.Loading(false)
            }
            .subscribe({
                upcomingMoviesResult.value = it
            }, {
                upcomingMoviesResult.value = ResultState.Error(it)
            })
            .autoDispose()
    }

    fun getTopRateMovies() {
        EspressoIdlingResource.increment()
        dataManager.getTopRateMovies()
            .doOnSubscribe {
                toprateMoviesResult.value = ResultState.Loading(true)
            }
            .doAfterTerminate {
                EspressoIdlingResource.decrement()
                toprateMoviesResult.value = ResultState.Loading(false)
            }
            .subscribe({
                toprateMoviesResult.value = it
            }, {
                toprateMoviesResult.value = ResultState.Error(it)
            })
            .autoDispose()
    }
}