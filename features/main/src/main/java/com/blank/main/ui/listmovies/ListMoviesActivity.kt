package com.blank.main.ui.listmovies

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import com.blank.core.base.BaseActivity
import com.blank.core.data.remote.CODE_POPULAR_MOVIES
import com.blank.core.data.remote.CODE_TOP_RATED_MOVIES
import com.blank.core.data.remote.CODE_UPCOMING_MOVIES
import com.blank.core.navigation.Navigator
import com.blank.core.utils.ERROR_NOCONNECTION
import com.blank.core.utils.KEY_REQ_MOVIES
import com.blank.core.utils.view.VerticalSpaceItemDecoration
import com.blank.main.BR
import com.blank.main.R
import com.blank.main.databinding.ActivityListMoviesBinding
import com.blank.main.ui.listmovies.adapter.ListMoviesAdapter
import com.blank.main.ui.listmovies.adapter.ListMoviesLoadStateAdapter
import com.blank.widget.utils.dpToPx
import com.blank.widget.utils.observe
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ListMoviesActivity : BaseActivity<ActivityListMoviesBinding, ListMoviesViewModel>() {
    override val viewModel by viewModels<ListMoviesViewModel>()
    override fun layoutId(): Int = R.layout.activity_list_movies
    override fun bindingVariable(): Int = BR.vmlistmovies

    @Inject
    lateinit var listMoviesAdapter: ListMoviesAdapter

    @Inject
    lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        listMoviesAdapter.addLoadStateListener(this::manageStateMovies)
        binding.rvListMovies.apply {
            setHasFixedSize(true)
            addItemDecoration(VerticalSpaceItemDecoration(context dpToPx 8))
            adapter = listMoviesAdapter.withLoadStateHeaderAndFooter(
                footer = ListMoviesLoadStateAdapter(listMoviesAdapter::retry),
                header = ListMoviesLoadStateAdapter(listMoviesAdapter::retry)
            )
        }
        listMoviesAdapter.setListener { movieItem, _ ->
            navigator.goToDetail(movieItem)
        }

        observe(viewModel.resultItems) {
            listMoviesAdapter.submitData(lifecycle, it)
        }
        val reqMovies = intent.extras?.getInt(KEY_REQ_MOVIES) ?: -1
        when (reqMovies) {
            CODE_POPULAR_MOVIES -> {
                title = getString(R.string.popular_movies)
            }
            CODE_TOP_RATED_MOVIES -> {
                title = getString(R.string.toprated_movies)
            }
            CODE_UPCOMING_MOVIES -> {
                title = getString(R.string.upcoming_movies)
            }
        }
        viewModel.getMovies(reqMovies)
    }

    private fun manageStateMovies(loadState: CombinedLoadStates) {
        val isLoading = loadState.source.refresh is LoadState.Loading
        binding.pbListMovies.isVisible = isLoading

        val errorState = loadState.source.append as? LoadState.Error
            ?: loadState.source.prepend as? LoadState.Error
            ?: loadState.append as? LoadState.Error
            ?: loadState.prepend as? LoadState.Error
            ?: loadState.source.refresh as? LoadState.Error

        if (errorState != null) {
            val msgError = errorState.error.message
            if (msgError == ERROR_NOCONNECTION) {
                showMsgError(msgError)
            } else {
                msgError?.let { showMsgError(it) }
            }
            Toast.makeText(
                this,
                "\uD83D\uDE28 Wooops $msgError",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun showMsgError(vararg s: String) {
        Snackbar
            .make(binding.root, s[0], Snackbar.LENGTH_INDEFINITE)
            .setAction(getString(R.string.retry)) {
                listMoviesAdapter.retry()
            }.show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}