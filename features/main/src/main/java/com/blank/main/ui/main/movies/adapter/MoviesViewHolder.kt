package com.blank.main.ui.main.movies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blank.core.data.remote.ENDPOINT_IMAGE_CUSTOM
import com.blank.main.databinding.ItemMovieBinding
import com.blank.models.local.Movies
import com.blank.widget.utils.setImageRounded

class MoviesViewHolder(private val v: ItemMovieBinding) : RecyclerView.ViewHolder(v.root) {
    companion object {
        fun create(parent: ViewGroup): MoviesViewHolder =
            MoviesViewHolder(
                ItemMovieBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
    }

    fun bind(resultsItem: Movies) {
        v.itemImg.setImageRounded(ENDPOINT_IMAGE_CUSTOM.plus(resultsItem.posterPath), 20f)
    }
}