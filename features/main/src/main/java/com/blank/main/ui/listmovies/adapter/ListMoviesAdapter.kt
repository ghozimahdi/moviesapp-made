package com.blank.main.ui.listmovies.adapter

import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.blank.core.utils.instrumenttest.EspressoIdlingResource
import com.blank.models.local.Movies
import javax.inject.Inject

class ListMoviesAdapter @Inject constructor() :
    PagingDataAdapter<Movies, ListMoviesViewHolder>(REPO_COMPARATOR) {

    private lateinit var listener: (Movies, viewArray: Array<View>) -> Unit

    fun setListener(listener: (Movies, viewArray: Array<View>) -> Unit) {
        this.listener = listener
    }

    override fun onBindViewHolder(holder: ListMoviesViewHolder, position: Int) {
        getItem(position)?.let { item ->
            holder.bind(item)
            holder.itemView.setOnClickListener {
                item.type = 0
                listener(item, arrayOf())
            }
        }
        if (!EspressoIdlingResource.getEspressoIdlingResource().isIdleNow) {
            EspressoIdlingResource.decrement()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListMoviesViewHolder =
        ListMoviesViewHolder.create(parent)

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<Movies>() {
            override fun areItemsTheSame(oldItem: Movies, newItem: Movies): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Movies, newItem: Movies): Boolean =
                oldItem == newItem

        }
    }
}