package com.blank.main.ui.detail

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.blank.core.base.BaseActivity
import com.blank.core.utils.KEY_ITEM_MOVIES
import com.blank.main.BR
import com.blank.main.R
import com.blank.main.databinding.ActivityDetailMovieBinding
import com.blank.models.local.Movies
import com.blank.models.local.RxEvent
import com.blank.widget.utils.observe
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

@AndroidEntryPoint
class DetailMovieActivity : BaseActivity<ActivityDetailMovieBinding, DetailMovieViewModel>() {

    override fun layoutId(): Int = R.layout.activity_detail_movie
    override fun bindingVariable(): Int = BR.vmdetail
    override val viewModel by viewModels<DetailMovieViewModel>()
    private var menu: Menu? = null
    private var icon = false
    private var model: Movies? = null

    @Inject
    lateinit var eventBus: EventBus

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
        supportActionBar?.title = getString(R.string.detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        model = intent.getParcelableExtra(KEY_ITEM_MOVIES) as Movies
        model.let { viewModel.setMovies(it) }

        observe(viewModel.saveResultState) {
            eventBus.post(model?.type?.let { it1 -> RxEvent.EventFavorite(it1) })
            if (it) {
                Toast.makeText(this, R.string.saveMoviesMsg, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, R.string.delMoviesMsg, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        observe(viewModel.isSave) {
            icon = it
            changeIcFavorite(menu)
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        this.menu = menu
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.i_save -> {
                model?.let { viewModel.saveToFavorite(icon, it) }
                menu?.let { changeIcFavorite(it) }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun changeIcFavorite(menu: Menu) {
        menu.findItem(R.id.i_save).icon =
            if (icon) {
                ContextCompat.getDrawable(this, R.drawable.ic_baseline_favorite_active)
            } else {
                ContextCompat.getDrawable(this, R.drawable.ic_baseline_favorite)
            }
        icon = !icon
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}
