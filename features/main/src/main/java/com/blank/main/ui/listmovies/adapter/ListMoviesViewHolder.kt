package com.blank.main.ui.listmovies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blank.main.databinding.ItemListmoviesBinding
import com.blank.models.local.Movies
import com.blank.widget.utils.setImageRounded

class ListMoviesViewHolder(private val v: ItemListmoviesBinding) :
    RecyclerView.ViewHolder(v.root) {

    companion object {
        fun create(parent: ViewGroup) =
            ListMoviesViewHolder(
                ItemListmoviesBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
    }

    fun bind(movies: Movies) {
        v.image.setImageRounded(movies.getPoster(), 20f)
        v.genre.text = movies.allGenres()
        v.title.text = movies.title
        v.tgl.text = movies.getDate()
    }
}