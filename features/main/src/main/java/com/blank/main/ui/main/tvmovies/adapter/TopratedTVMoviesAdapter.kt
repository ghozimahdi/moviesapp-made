package com.blank.main.ui.main.tvmovies.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blank.models.local.Movies
import javax.inject.Inject

class TopratedTVMoviesAdapter @Inject constructor() :
    RecyclerView.Adapter<TvMoviesViewHolder>() {
    private val data: MutableList<Movies> = mutableListOf()
    private var listener: ((Movies) -> Unit)? = null

    fun setListener(listener: ((Movies) -> Unit)) {
        this.listener = listener
    }

    fun setData(list: MutableList<Movies>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvMoviesViewHolder =
        TvMoviesViewHolder.create(parent)

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: TvMoviesViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item)
        holder.itemView.setOnClickListener {
            item.type = 1
            listener?.invoke(item)
        }
    }
}