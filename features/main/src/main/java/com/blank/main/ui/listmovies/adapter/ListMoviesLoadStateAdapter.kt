package com.blank.main.ui.listmovies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.blank.main.databinding.ItemLoadStateFooterViewBinding

class ListMoviesLoadStateAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<ListMoviesLoadStateViewHolder>() {
    override fun onBindViewHolder(holder: ListMoviesLoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ) = ListMoviesLoadStateViewHolder.create(parent, retry)
}

class ListMoviesLoadStateViewHolder(
    private val binding: ItemLoadStateFooterViewBinding,
    retry: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    init {
        binding.retryButton.setOnClickListener { retry() }
    }

    fun bind(loadState: LoadState) {
        if (loadState is LoadState.Error)
            binding.errorMsg.text = loadState.error.localizedMessage

        binding.progressBar.isVisible = loadState is LoadState.Loading
        binding.retryButton.isVisible = loadState is LoadState.Error
        binding.errorMsg.isVisible = loadState is LoadState.Error
    }

    companion object {
        fun create(parent: ViewGroup, retry: () -> Unit) =
            ListMoviesLoadStateViewHolder(
                ItemLoadStateFooterViewBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ), retry
            )
    }
}