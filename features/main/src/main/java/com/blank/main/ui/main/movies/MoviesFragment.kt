package com.blank.main.ui.main.movies

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.blank.core.base.BaseFragment
import com.blank.core.navigation.Navigator
import com.blank.core.utils.KEY_REQ_MOVIES
import com.blank.core.utils.api.ResultState
import com.blank.core.utils.view.HorizontalSpaceItemDecoration
import com.blank.core.utils.view.PeekingLinearLayoutManager
import com.blank.main.BR
import com.blank.main.R
import com.blank.main.databinding.FragmentMoviesBinding
import com.blank.main.ui.listmovies.ListMoviesActivity
import com.blank.main.ui.main.movies.adapter.PopularMoviesAdapter
import com.blank.main.ui.main.movies.adapter.TopratedMoviesAdapter
import com.blank.main.ui.main.movies.adapter.UpcomingMoviesAdapter
import com.blank.models.local.Movies
import com.blank.widget.utils.dpToPx
import com.blank.widget.utils.observe
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class MoviesFragment : BaseFragment<FragmentMoviesBinding, MoviesViewModel>() {

    override fun layoutId(): Int = R.layout.fragment_movies
    override fun bindingVariable(): Int = BR.vmmovies
    override val viewModel by viewModels<MoviesViewModel>()

    @Inject
    lateinit var popularAdapter: PopularMoviesAdapter

    @Inject
    lateinit var topratedMoviesAdapter: TopratedMoviesAdapter

    @Inject
    lateinit var upcomingMoviesAdapter: UpcomingMoviesAdapter

    @Inject
    lateinit var navigator: Navigator

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.rvPopular?.apply {
            layoutManager =
                PeekingLinearLayoutManager(
                    requireContext(),
                    0.38f,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            addItemDecoration(HorizontalSpaceItemDecoration(requireContext() dpToPx 8))
            adapter = popularAdapter
        }
        popularAdapter.setListener(navigator::goToDetail)

        binding?.rvTopRate?.apply {
            layoutManager =
                PeekingLinearLayoutManager(
                    requireContext(),
                    0.38f,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            addItemDecoration(HorizontalSpaceItemDecoration(requireContext() dpToPx 8))
            adapter = topratedMoviesAdapter
        }
        topratedMoviesAdapter.setListener(navigator::goToDetail)

        binding?.rvUpcoming?.apply {
            layoutManager =
                PeekingLinearLayoutManager(
                    requireContext(),
                    0.38f,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            addItemDecoration(HorizontalSpaceItemDecoration(requireContext() dpToPx 8))
            adapter = upcomingMoviesAdapter
        }
        upcomingMoviesAdapter.setListener(navigator::goToDetail)

        observe(viewModel.popularMoviesResult, this::managePopularMoviesResult)
        observe(viewModel.toprateMoviesResult, this::manageTopratedMoviesResult)
        observe(viewModel.upcomingMoviesResult, this::manageUpcomingMoviesResult)

        viewModel.getPopularMovies()
        viewModel.getTopRateMovies()
        viewModel.getUpComingMovies()

        arrayOf(
            binding?.viewallPopular,
            binding?.viewallTopRated,
            binding?.viewallUpcoming
        ).forEachIndexed { index, values ->
            values?.setOnClickListener {
                Intent(requireContext(), ListMoviesActivity::class.java).apply {
                    putExtra(KEY_REQ_MOVIES, index)
                    startActivity(this)
                }
            }
        }
    }

    private fun managePopularMoviesResult(state: ResultState) {
        when (state) {
            is ResultState.Error -> {
                state.e.printStackTrace()
                val msg = state.e.message.toString()
                Timber.e(msg)
                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            }
            is ResultState.Loading -> {
                binding?.pbMoviesPopular?.isVisible = state.isLoading
            }
            is ResultState.Success<*> -> {
                popularAdapter.setData(state.data as MutableList<Movies>)
            }
        }
    }

    private fun manageTopratedMoviesResult(state: ResultState) {
        when (state) {
            is ResultState.Error -> {
                state.e.printStackTrace()
                val msg = state.e.message.toString()
                Timber.e(msg)
                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            }
            is ResultState.Loading -> {
                binding?.pbMoviesToprated?.isVisible = state.isLoading
            }
            is ResultState.Success<*> -> {
                topratedMoviesAdapter.setData(state.data as MutableList<Movies>)
            }
        }
    }

    private fun manageUpcomingMoviesResult(state: ResultState) {
        when (state) {

            is ResultState.Error -> {
                state.e.printStackTrace()
                val msg = state.e.message.toString()
                Timber.e(msg)
                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            }
            is ResultState.Loading -> {
                binding?.pbMoviesUpcoming?.isVisible = state.isLoading
            }
            is ResultState.Success<*> -> {
                upcomingMoviesAdapter.setData(state.data as MutableList<Movies>)
            }
        }
    }

    override fun onDestroyView() {
        binding?.apply {
            rvPopular.adapter = null
            rvTopRate.adapter = null
            rvUpcoming.adapter = null
        }
        super.onDestroyView()
    }
}