package com.blank.main.ui.listmovies

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.rxjava2.cachedIn
import com.blank.core.base.BaseViewModel
import com.blank.core.utils.instrumenttest.EspressoIdlingResource
import com.blank.main.ui.listmovies.source.ListMoviesSource
import com.blank.models.local.Movies
import io.reactivex.Flowable

class ListMoviesViewModel @ViewModelInject constructor(
    private val listMoviesSource: ListMoviesSource
) : BaseViewModel() {

    private var currentMovieItems: Flowable<PagingData<Movies>>? = null
    val resultItems = MutableLiveData<PagingData<Movies>>()

    private fun currentMovieItems(req: Int): Flowable<PagingData<Movies>> {
        val lastResult = currentMovieItems
        if (lastResult != null) return lastResult

        val newResult = listMoviesSource.getPager(req)
        newResult.cachedIn(viewModelScope)

        currentMovieItems = newResult
        return newResult
    }

    fun getMovies(req: Int) {
        EspressoIdlingResource.increment()
        currentMovieItems(req)
            .subscribe {
                resultItems.value = it
            }.autoDispose()
    }
}