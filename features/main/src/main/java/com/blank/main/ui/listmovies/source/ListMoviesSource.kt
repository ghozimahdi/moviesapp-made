package com.blank.main.ui.listmovies.source

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.rxjava2.RxPagingSource
import androidx.paging.rxjava2.flowable
import com.blank.core.data.DataManager
import com.blank.core.utils.ERROR_NOCONNECTION
import com.blank.core.utils.NETWORK_PAGE_SIZE
import com.blank.core.utils.NetworkHelper
import com.blank.models.local.Movies
import io.reactivex.Single
import javax.inject.Inject

private const val MOVIES_STARTING_PAGE_INDEX = 1

class ListMoviesSource @Inject constructor(
    private val dataManager: DataManager,
    private val networkHelper: NetworkHelper
) : RxPagingSource<Int, Movies>() {
    private var reqCodeMovies = -1

    fun getPager(req: Int) = Pager(
        config = PagingConfig(
            pageSize = NETWORK_PAGE_SIZE,
            initialLoadSize = NETWORK_PAGE_SIZE,
            prefetchDistance = 5,
            enablePlaceholders = false
        ), pagingSourceFactory =
        { setReqCodeMovies(req) }
    ).flowable

    private fun setReqCodeMovies(req: Int) = apply {
        reqCodeMovies = req
    }

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, Movies>> {
        val position = params.key ?: MOVIES_STARTING_PAGE_INDEX
        return when (networkHelper.isNetworkConnected()) {
            true -> dataManager.getMovies(reqCodeMovies, position)
                .toObservable()
                .flatMapIterable { it }
                .flatMap { moviesItem ->
                    val item = moviesItem.copy()
                    dataManager.getGenres(0, item.genreIds)
                        .toObservable()
                        .map {
                            item.genres = it
                            item
                        }
                }
                .toList()
                .map { toLoadResult(it, position) }
                .onErrorReturn { LoadResult.Error(it) }
            false -> Single.just(LoadResult.Error(Throwable(ERROR_NOCONNECTION)))
        }
    }

    private fun toLoadResult(
        data: List<Movies>,
        position: Int
    ): LoadResult<Int, Movies> = LoadResult.Page(
        data = data,
        prevKey = if (position == MOVIES_STARTING_PAGE_INDEX) null else position,
        nextKey = if (data.isEmpty()) null else position.plus(1)
    )
}