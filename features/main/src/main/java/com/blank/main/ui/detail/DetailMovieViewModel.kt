package com.blank.main.ui.detail

import android.os.Handler
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.blank.core.base.BaseViewModel
import com.blank.core.data.DataManager
import com.blank.core.utils.instrumenttest.EspressoIdlingResource
import com.blank.models.local.Movies
import com.blank.models.local.entity.MovieEntity

class DetailMovieViewModel @ViewModelInject constructor(private val dataManager: DataManager) :
    BaseViewModel() {

    private var _movies = MutableLiveData<Movies>()
    val movies get() = _movies
    val isLoading = MutableLiveData<Boolean>()
    val saveResultState = MutableLiveData<Boolean>()
    val isSave = MutableLiveData<Boolean>()

    fun setMovies(movies: Movies?) {
        EspressoIdlingResource.increment()
        isLoading.postValue(true)
        Handler().postDelayed({
            isLoading.postValue(false)
            EspressoIdlingResource.decrement()
            _movies.value = movies
        }, 1000)

        movies?.id?.let {
            dataManager.fetchMoviesById(it)
                .subscribe({ isExists ->
                    isSave.value = isExists
                }, {
                    isSave.value = false
                })
                .autoDispose()

        }
    }

    fun saveToFavorite(save: Boolean, movies: Movies) {
        val movieEntity = MovieEntity.mappingMoviesEntity(movies)
        if (save) {
            dataManager.insertMovie(movieEntity)
                .subscribe {
                    saveResultState.value = it
                }
                .autoDispose()
        } else {
            dataManager.deleteMovie(movieEntity)
                .subscribe {
                    saveResultState.value = false
                }
                .autoDispose()
        }
    }
}