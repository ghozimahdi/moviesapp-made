package com.blank.favorite.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.blank.favorite.ui.movies.FavoriteMoviesFragment
import com.blank.favorite.ui.tvmovies.FavoriteTvMoviesFragment

class FavoriteViewPagerAdapter(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {
    private val fragments = arrayOf(FavoriteMoviesFragment(), FavoriteTvMoviesFragment())
    override fun getItemCount(): Int = fragments.size
    override fun createFragment(position: Int): Fragment = fragments[position]
}