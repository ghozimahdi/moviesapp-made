package com.blank.favorite.utils

import androidx.fragment.app.Fragment
import com.blank.favorite.di.DaggerFavoriteComponent
import com.blank.favorite.ui.FavoriteFragment
import com.blank.favorite.ui.movies.FavoriteMoviesFragment
import com.blank.favorite.ui.tvmovies.FavoriteTvMoviesFragment
import com.blank.framework.di.dfm.CoreModuleDependencies
import dagger.hilt.android.EntryPointAccessors

fun <T : Fragment> Fragment.injectComponentDaggerHilt(fragment: T) {
    val coreModuleDependencies = EntryPointAccessors.fromApplication(
        requireActivity().applicationContext,
        CoreModuleDependencies::class.java
    )

    val daggerComponent = DaggerFavoriteComponent.factory()
        .create(coreModuleDependencies)

    when (fragment) {
        is FavoriteFragment -> {
            daggerComponent.inject(fragment)
        }
        is FavoriteMoviesFragment -> {
            daggerComponent.inject(fragment)
        }
        is FavoriteTvMoviesFragment -> {
            daggerComponent.inject(fragment)
        }
    }
}