package com.blank.favorite.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.blank.favorite.R
import com.blank.favorite.databinding.FragmentFavoriteBinding
import com.blank.favorite.utils.injectComponentDaggerHilt
import com.blank.widget.utils.setupWithViewPager

class FavoriteFragment : Fragment() {
    private var binding: FragmentFavoriteBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        injectComponentDaggerHilt(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavoriteBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.viewpagerFavorite?.apply {
            adapter = FavoriteViewPagerAdapter(requireActivity())
            isUserInputEnabled = false
            binding?.tabsFavorite?.setupWithViewPager(
                this,
                resources.getStringArray(R.array.title_favorite)
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding?.viewpagerFavorite?.apply {
            adapter = null
        }
        binding = null
    }
}