package com.blank.favorite.ui.movies

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.blank.favorite.databinding.ItemListmoviesFavoriteBinding
import com.blank.models.local.Movies
import com.blank.widget.utils.setImageRounded
import com.chauthai.swipereveallayout.ViewBinderHelper

class FavoriteMoviesAdapter(
    private val listenerDelete: (Movies, (Movies, Int) -> Unit, Int) -> Unit,
    private val data: MutableList<Movies> = mutableListOf()
) : RecyclerView.Adapter<FavoriteMoviesAdapter.MoviesFavoriteViewHolder>() {

    private val viewBinderHelper = ViewBinderHelper()
    private lateinit var listener: (Movies, viewArray: Array<View>) -> Unit
    private val _liveData = MutableLiveData<List<Movies>>()
    val liveData: LiveData<List<Movies>> get() = _liveData

    fun setListener(listener: (Movies, viewArray: Array<View>) -> Unit) {
        this.listener = listener
    }

    fun setData(data: List<Movies>) {
        this.data.clear()
        this.data.addAll(data)
        _liveData.value = data
        notifyDataSetChanged()
    }

    fun add(resultsItem: Movies, position: Int) {
        data.add(position, resultsItem)
        _liveData.value = data
        notifyItemInserted(position)
        notifyItemRangeChanged(position, data.size)
    }

    private fun removeAt(position: Int) {
        data.removeAt(position)
        _liveData.value = data
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, data.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesFavoriteViewHolder =
        create(parent)

    private fun create(parent: ViewGroup) =
        MoviesFavoriteViewHolder(
            ItemListmoviesFavoriteBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: MoviesFavoriteViewHolder, position: Int) {
        val item = data[position]
        viewBinderHelper.setOpenOnlyOne(true)
        viewBinderHelper.bind(holder.v.swipelayout, item.title)
        viewBinderHelper.closeLayout(item.title)
        holder.bind(item, position, this::removeAt)
    }

    inner class MoviesFavoriteViewHolder(val v: ItemListmoviesFavoriteBinding) :
        RecyclerView.ViewHolder(v.root) {

        fun bind(
            resultsItem: Movies,
            position: Int,
            removeAt: (Int) -> Unit
        ) {
            v.image.setImageRounded(resultsItem.getPoster(), 20f)
            v.genre.text = resultsItem.allGenres()
            v.title.text = resultsItem.title
            v.tgl.text = resultsItem.getDate()

            v.tvDelete.setOnClickListener {
                removeAt(position)
                listenerDelete(resultsItem, this@FavoriteMoviesAdapter::add, position)
            }

            v.iCListMovies.setOnClickListener {
                resultsItem.type = 0
                listener(resultsItem, arrayOf())
            }
        }
    }
}