package com.blank.favorite.ui.movies

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.blank.core.base.BaseFragment
import com.blank.core.base.ViewModelFactory
import com.blank.core.navigation.Navigator
import com.blank.core.utils.view.VerticalSpaceItemDecoration
import com.blank.favorite.BR
import com.blank.favorite.R
import com.blank.favorite.databinding.FragmentFavoriteMoviesBinding
import com.blank.favorite.utils.injectComponentDaggerHilt
import com.blank.models.local.RxEvent
import com.blank.widget.utils.dpToPx
import com.blank.widget.utils.hide
import com.blank.widget.utils.observe
import com.blank.widget.utils.show
import com.google.android.material.snackbar.Snackbar
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import javax.inject.Inject

class FavoriteMoviesFragment :
    BaseFragment<FragmentFavoriteMoviesBinding, FavoriteMoviesViewModel>() {

    @Inject
    lateinit var factory: ViewModelFactory<FavoriteMoviesViewModel>

    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var eventBus: EventBus
    private var onRefresh = false

    override val viewModel by viewModels<FavoriteMoviesViewModel> { factory }

    override fun layoutId(): Int = R.layout.fragment_favorite_movies
    override fun bindingVariable(): Int = BR.vmfavoritemovies

    override fun onCreate(savedInstanceState: Bundle?) {
        injectComponentDaggerHilt(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onRefresh = false

        val favoriteMoviesAdapter = FavoriteMoviesAdapter({ item, addItem, position ->
            binding?.parentMovies?.let { parentMovies ->
                Snackbar
                    .make(parentMovies, getText(R.string.delMoviesMsg), Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.undo)) {
                        addItem(item, position)
                    }.addCallback(object : Snackbar.Callback() {
                        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                            super.onDismissed(transientBottomBar, event)
                            when (event) {
                                DISMISS_EVENT_CONSECUTIVE -> {
                                    deleteMovie()
                                }

                                DISMISS_EVENT_TIMEOUT -> {
                                    deleteMovie()
                                }
                            }
                        }

                        private fun deleteMovie() {
                            viewModel.deleteMoviesFavorite(item)
                        }
                    }).show()
            }
        })

        binding?.rvMoviesFavorit?.apply {
            setHasFixedSize(true)
            addItemDecoration(VerticalSpaceItemDecoration(context dpToPx 8))
            adapter = favoriteMoviesAdapter
        }
        favoriteMoviesAdapter.setListener { movieItem, _ ->
            navigator.goToDetail(movieItem)
        }

        observe(viewModel.mutableLiveFetchMovies) {
            favoriteMoviesAdapter.setData(it)
        }

        observe(favoriteMoviesAdapter.liveData) {
            if (it.isEmpty()) {
                binding?.groupEmptData?.show()
            } else {
                binding?.groupEmptData?.hide()
            }
        }

        viewModel.fetchMovies()
    }

    @Subscribe
    fun onTypeMovies(type: RxEvent.EventFavorite) {
        if (type.type == 0 && thisFavoriteFragment()) {
            onRefresh = true
        }
    }

    override fun onResume() {
        super.onResume()
        if (!eventBus.isRegistered(this) && thisFavoriteFragment()) {
            eventBus.register(this)
        }

        if (onRefresh && thisFavoriteFragment()) {
            viewModel.fetchMovies()
            onRefresh = !onRefresh
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        eventBus.unregister(this)
    }

    private fun thisFavoriteFragment(): Boolean =
        findNavController().currentDestination?.id == R.id.favoriteFragment
}