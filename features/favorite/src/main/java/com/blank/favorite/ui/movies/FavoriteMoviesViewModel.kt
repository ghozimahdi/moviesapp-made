package com.blank.favorite.ui.movies

import androidx.lifecycle.MutableLiveData
import com.blank.core.base.BaseViewModel
import com.blank.core.data.DataManager
import com.blank.models.local.Movies
import com.blank.models.local.entity.MovieEntity
import javax.inject.Inject

class FavoriteMoviesViewModel @Inject constructor(private val dataManager: DataManager) :
    BaseViewModel() {

    val mutableLiveFetchMovies = MutableLiveData<List<Movies>>()

    fun fetchMovies() {
        dataManager.fetchMoviesByType(0)
            .subscribe {
                mutableLiveFetchMovies.value = Movies.mappingMovies(it)
            }
            .autoDispose()
    }

    fun deleteMoviesFavorite(movies: Movies) {
        dataManager.deleteMovie(MovieEntity.mappingMoviesEntity(movies))
            .subscribe()
            .autoDispose()
    }
}
