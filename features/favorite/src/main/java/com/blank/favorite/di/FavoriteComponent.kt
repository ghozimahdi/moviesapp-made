package com.blank.favorite.di

import com.blank.favorite.ui.FavoriteFragment
import com.blank.favorite.ui.movies.FavoriteMoviesFragment
import com.blank.favorite.ui.tvmovies.FavoriteTvMoviesFragment
import com.blank.framework.di.dfm.CoreModuleDependencies
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [CoreModuleDependencies::class]
)
interface FavoriteComponent {
    @Component.Factory
    interface Factory {
        fun create(dependencies: CoreModuleDependencies): FavoriteComponent
    }

    fun inject(fragment: FavoriteFragment)
    fun inject(fragment: FavoriteMoviesFragment)
    fun inject(fragment: FavoriteTvMoviesFragment)
}
