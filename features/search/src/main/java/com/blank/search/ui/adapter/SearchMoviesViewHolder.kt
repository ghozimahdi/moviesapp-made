package com.blank.search.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blank.models.local.Movies
import com.blank.search.databinding.ItemSearchBinding
import com.blank.widget.utils.setImageRounded

class SearchMoviesViewHolder(private val v: ItemSearchBinding) :
    RecyclerView.ViewHolder(v.root) {

    companion object {
        fun create(parent: ViewGroup) =
            SearchMoviesViewHolder(
                ItemSearchBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
    }

    fun bind(movies: Movies) {
        v.image.setImageRounded(movies.getPoster(), 20f)
        v.genre.text = movies.allGenres()
        v.title.text = movies.title
        v.tgl.text = movies.getDate()
    }
}