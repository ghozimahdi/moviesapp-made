package com.blank.search.ui.source

import androidx.paging.rxjava2.RxPagingSource
import com.blank.core.data.DataManager
import com.blank.core.utils.ERROR_NOCONNECTION
import com.blank.core.utils.NO_ITEM_PAGING
import com.blank.core.utils.NetworkHelper
import com.blank.models.local.Movies
import io.reactivex.Single

private const val MOVIES_STARTING_PAGE_INDEX = 1

class SearchMoviesSource(
    private val dataManager: DataManager,
    private val networkHelper: NetworkHelper,
    private val keyword: String
) : RxPagingSource<Int, Movies>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, Movies>> {
        val position = params.key ?: MOVIES_STARTING_PAGE_INDEX
        return when (networkHelper.isNetworkConnected()) {
            true -> dataManager.searchMovies(keyword, position)
                .toObservable()
                .flatMapIterable { it }
                .flatMap { moviesItem ->
                    val item = moviesItem.copy()
                    dataManager.getGenres(0, item.genreIds)
                        .toObservable()
                        .map {
                            item.genres = it
                            item
                        }
                }
                .toList()
                .map {
                    if (it.size <= 0)
                        toLoadResultError()
                    else
                        toLoadResult(it, position)
                }
                .onErrorReturn { LoadResult.Error(it) }
            false -> Single.just(LoadResult.Error(Throwable(ERROR_NOCONNECTION)))
        }
    }

    private fun toLoadResultError(): LoadResult<Int, Movies> = LoadResult.Error(
        Throwable(
            NO_ITEM_PAGING
        )
    )

    private fun toLoadResult(
        data: List<Movies>,
        position: Int
    ): LoadResult<Int, Movies> = LoadResult.Page(
        data = data,
        prevKey = if (position == MOVIES_STARTING_PAGE_INDEX) null else position,
        nextKey = if (data.isEmpty()) null else position.plus(1)
    )
}