package com.blank.search.ui

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import com.blank.core.base.BaseActivity
import com.blank.core.base.ViewModelFactory
import com.blank.core.navigation.Navigator
import com.blank.core.utils.ERROR_NOCONNECTION
import com.blank.core.utils.view.VerticalSpaceItemDecoration
import com.blank.search.BR
import com.blank.search.R
import com.blank.search.databinding.ActivitySearchBinding
import com.blank.search.ui.adapter.SearchMoviesAdapter
import com.blank.search.ui.adapter.SearchMoviesLoadStateAdapter
import com.blank.search.utils.injectComponentDaggerHilt
import com.blank.widget.utils.dpToPx
import com.blank.widget.utils.hide
import com.blank.widget.utils.observe
import com.blank.widget.utils.show
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

class SearchActivity : BaseActivity<ActivitySearchBinding, SearchViewModel>() {

    @Inject
    lateinit var factory: ViewModelFactory<SearchViewModel>

    @Inject
    lateinit var searchMoviesAdapter: SearchMoviesAdapter

    @Inject
    lateinit var navigator: Navigator

    private var snackbar: Snackbar? = null

    override fun layoutId(): Int = R.layout.activity_search
    override fun bindingVariable(): Int = BR.vmsearch
    override val viewModel: SearchViewModel by viewModels { factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectComponentDaggerHilt(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        searchMoviesAdapter.addLoadStateListener(this::manageStateSearchMovies)
        binding.rvSearch.apply {
            setHasFixedSize(true)
            addItemDecoration(VerticalSpaceItemDecoration(context dpToPx 8))
            adapter = searchMoviesAdapter.withLoadStateHeaderAndFooter(
                footer = SearchMoviesLoadStateAdapter(searchMoviesAdapter::retry),
                header = SearchMoviesLoadStateAdapter(searchMoviesAdapter::retry)
            )
        }

        searchMoviesAdapter.setListener { movieItem, _ ->
            navigator.goToDetail(movieItem)
        }

        observe(viewModel.resultsItems) {
            searchMoviesAdapter.submitData(lifecycle, it)
        }
    }

    private fun manageStateSearchMovies(loadState: CombinedLoadStates) {
        val isLoading = loadState.source.refresh is LoadState.Loading
        binding.pbSearch.isVisible = isLoading

        val errorState = loadState.source.append as? LoadState.Error
            ?: loadState.source.prepend as? LoadState.Error
            ?: loadState.append as? LoadState.Error
            ?: loadState.prepend as? LoadState.Error
            ?: loadState.source.refresh as? LoadState.Error

        val notLoading = loadState.source.refresh as? LoadState.NotLoading
        val isendOfPaginationReached = notLoading?.endOfPaginationReached ?: true
        if (isLoading) {
            binding.rvSearch.hide()
        } else if (!isendOfPaginationReached) {
            if (snackbar != null)
                snackbar?.dismiss()
            binding.rvSearch.show()
        }

        if (errorState != null) {
            errorState.error.printStackTrace()
            var msgError = errorState.error.message
            if (searchMoviesAdapter.itemCount <= 0) {
                if (msgError == ERROR_NOCONNECTION) {
                    msgError = getString(R.string.no_internet_conten)
                }
            }

            if (!binding.rvSearch.isVisible && !isLoading) {
                if (snackbar == null)
                    snackbar =
                        Snackbar.make(
                            binding.parentSearch,
                            msgError.toString(),
                            Snackbar.LENGTH_INDEFINITE
                        )
                            .setAction(getString(R.string.retry)) {
                                searchMoviesAdapter.retry()
                            }
                snackbar?.show()
            }

            Toast.makeText(
                applicationContext,
                "\uD83D\uDE28 Wooops $msgError",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.iSearch).actionView as SearchView
        searchView.onActionViewExpanded()
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.queryHint = getString(R.string.hintSearchMovie)
        searchView.maxWidth = Integer.MAX_VALUE
        viewModel.searchMovies(searchView)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}