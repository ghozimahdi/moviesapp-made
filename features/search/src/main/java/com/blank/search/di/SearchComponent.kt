package com.blank.search.di

import com.blank.framework.di.dfm.CoreModuleDependencies
import com.blank.search.ui.SearchActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [CoreModuleDependencies::class]
)
interface SearchComponent {
    @Component.Factory
    interface Factory {
        fun create(dependencies: CoreModuleDependencies): SearchComponent
    }

    fun inject(activity: SearchActivity)
}
