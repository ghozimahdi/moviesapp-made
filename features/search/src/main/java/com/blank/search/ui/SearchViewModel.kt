package com.blank.search.ui

import androidx.appcompat.widget.SearchView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.rxjava2.cachedIn
import androidx.paging.rxjava2.flowable
import com.blank.core.base.BaseViewModel
import com.blank.core.data.DataManager
import com.blank.core.utils.NETWORK_PAGE_SIZE
import com.blank.core.utils.NetworkHelper
import com.blank.core.utils.rx.observableIo
import com.blank.models.local.Movies
import com.blank.search.ui.source.SearchMoviesSource
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class SearchViewModel @Inject constructor(
    private val dataManager: DataManager,
    private val networkHelper: NetworkHelper
) : BaseViewModel() {

    val resultsItems = MutableLiveData<PagingData<Movies>>()

    private fun pagerSearchItems(keyword: String): Flowable<PagingData<Movies>> =
        Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                initialLoadSize = NETWORK_PAGE_SIZE,
                prefetchDistance = 5,
                enablePlaceholders = false
            ), pagingSourceFactory =
            { SearchMoviesSource(dataManager, networkHelper, keyword) }
        ).flowable.cachedIn(viewModelScope)

    fun searchMovies(searchView: SearchView) {
        searchView.queryTextChanges()
            .filter { it.isNotEmpty() && it.length >= 3 && it.isNotBlank() }
            .debounce(300, TimeUnit.MILLISECONDS)
            .observableIo()
            .toFlowable(BackpressureStrategy.BUFFER)
            .flatMap(this::pagerSearchItems)
            .subscribe {
                resultsItems.value = it
            }.autoDispose()
    }

    private fun SearchView.queryTextChanges(): Observable<String> =
        Observable.create { subscriber ->
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextChange(newText: String): Boolean {
                    subscriber.onNext(newText)
                    return false
                }

                override fun onQueryTextSubmit(query: String): Boolean {
                    return false
                }
            })
        }
}