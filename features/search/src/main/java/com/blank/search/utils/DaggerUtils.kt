package com.blank.search.utils

import androidx.appcompat.app.AppCompatActivity
import com.blank.framework.di.dfm.CoreModuleDependencies
import com.blank.search.di.DaggerSearchComponent
import com.blank.search.ui.SearchActivity
import dagger.hilt.android.EntryPointAccessors

fun AppCompatActivity.injectComponentDaggerHilt(searchActivity: SearchActivity) {
    val coreModuleDependencies = EntryPointAccessors.fromApplication(
        applicationContext,
        CoreModuleDependencies::class.java
    )

    val daggerComponent = DaggerSearchComponent.factory()
        .create(coreModuleDependencies)
    daggerComponent.inject(searchActivity)
}