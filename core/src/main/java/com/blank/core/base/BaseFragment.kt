package com.blank.core.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

/**
 * Created by knalb on 04/10/18.
 * Email : -
 * No Tpln : -
 * Profesi : -
 */
abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel> :
    Fragment() {

    protected var binding: T? = null

    @LayoutRes
    abstract fun layoutId(): Int
    abstract fun bindingVariable(): Int
    abstract val viewModel: V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layoutId(), container, false)
        return binding?.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding?.setVariable(bindingVariable(), viewModel)
        binding?.executePendingBindings()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}