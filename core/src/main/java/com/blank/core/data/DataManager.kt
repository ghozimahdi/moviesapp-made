package com.blank.core.data

import com.blank.core.data.local.db.DbHelper
import com.blank.core.data.remote.ApiHelper
import com.blank.models.local.Movies
import io.reactivex.Single

/**
 * Created by GhoziMahdi on 09/12/2019.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */
interface DataManager : ApiHelper, DbHelper {
    fun getMovies(typeMovie: Int, page: Int): Single<List<Movies>>
    fun getTvMovies(typeMovie: Int, page: Int): Single<List<Movies>>
}