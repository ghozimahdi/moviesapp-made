package com.blank.core.data.remote

import com.blank.models.remote.MsgGenres
import com.blank.models.remote.ResponseMovies
import io.reactivex.Flowable
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Url

/**
 * Created by GhoziMahdi on 23/09/2018.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */

interface ApiService {
    @GET
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun doGetMovies(@Url url: String): Flowable<ResponseMovies>

    @GET
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun doGetGenresMovies(@Url url: String): Observable<MsgGenres>
}
