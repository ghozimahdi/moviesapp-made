package com.blank.core.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.blank.core.data.local.db.dao.MovieDao
import com.blank.models.local.entity.MovieEntity
import com.blank.models.remote.Converters

/**
 * Created by GhoziMahdi on 09/12/2019.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */

@Database(entities = [MovieEntity::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}