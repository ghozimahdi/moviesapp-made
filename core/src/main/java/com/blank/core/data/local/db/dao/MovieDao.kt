package com.blank.core.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery
import com.blank.models.local.entity.MovieEntity
import io.reactivex.Single

/**
 * Created by GhoziMahdi on 09/12/2019.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */

@Dao
interface MovieDao {
    @Delete
    fun delete(movie: MovieEntity)

    @Query("SELECT * FROM movies WHERE id LIKE :id LIMIT 1")
    fun findById(id: Int): Single<MovieEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: MovieEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(movies: List<MovieEntity>)

    @Query("SELECT * FROM movies")
    fun loadAll(): Single<List<MovieEntity>>

    @Query("SELECT * FROM movies WHERE id IN (:userIds)")
    fun loadAllByIds(userIds: List<Int>): List<MovieEntity>

    @RawQuery
    fun loadAll(query: SupportSQLiteQuery): List<MovieEntity>

    @RawQuery(observedEntities = [MovieEntity::class])
    fun loadAllLive(query: SupportSQLiteQuery): LiveData<List<MovieEntity>>
}