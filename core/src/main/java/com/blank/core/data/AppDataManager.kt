package com.blank.core.data

import androidx.lifecycle.LiveData
import com.blank.core.data.local.db.DbHelper
import com.blank.core.data.remote.*
import com.blank.core.utils.api.ResultState
import com.blank.models.local.Genres
import com.blank.models.local.Movies
import com.blank.models.local.entity.MovieEntity
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by GhoziMahdi on 14/10/2019.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */
@Singleton
class AppDataManager @Inject constructor(
    private val apiHelper: ApiHelper,
    private val dbHelper: DbHelper
) : DataManager {
    override fun getMovies(typeMovie: Int, page: Int): Single<List<Movies>> =
        when (typeMovie) {
            CODE_POPULAR_MOVIES -> {
                apiHelper.getPopularMovies(page)
            }
            CODE_TOP_RATED_MOVIES -> {
                apiHelper.getTopRateMovies(page)
            }
            CODE_UPCOMING_MOVIES -> {
                apiHelper.getUpComingMovies(page)
            }
            else -> Single.error(Throwable(ERROR_CODE_MOVIES))
        }

    override fun getTvMovies(typeMovie: Int, page: Int): Single<List<Movies>> =
        when (typeMovie) {
            CODE_POPULAR_MOVIES -> {
                apiHelper.getPopularTVMovies(page)
            }
            CODE_TOP_RATED_MOVIES -> {
                apiHelper.getTopratedTVMovies(page)
            }
            else -> Single.error(Throwable(ERROR_CODE_MOVIES))
        }

    override fun getPopularTVMovies(page: Int): Single<List<Movies>> =
        apiHelper.getPopularTVMovies(page)

    override fun getPopularTVMovies(): Single<ResultState> = apiHelper.getPopularTVMovies()
    override fun getTopratedTVMovies(page: Int): Single<List<Movies>> =
        apiHelper.getTopratedTVMovies(page)

    override fun getTopratedTVMovies(): Single<ResultState> = apiHelper.getTopratedTVMovies()

    override fun getPopularMovies(page: Int): Single<List<Movies>> =
        apiHelper.getPopularMovies(page)

    override fun getPopularMovies(): Single<ResultState> = apiHelper.getPopularMovies()

    override fun getTopRateMovies(page: Int): Single<List<Movies>> =
        apiHelper.getTopRateMovies(page)

    override fun getTopRateMovies(): Single<ResultState> =
        apiHelper.getTopRateMovies()

    override fun getUpComingMovies(page: Int): Single<List<Movies>> =
        apiHelper.getUpComingMovies(page)

    override fun getUpComingMovies(): Single<ResultState> =
        apiHelper.getUpComingMovies()

    override fun getGenres(type: Int, id: List<Int>): Single<MutableList<Genres>> =
        apiHelper.getGenres(type, id)

    override fun searchMovies(keyword: String, page: Int): Single<List<Movies>> =
        apiHelper.searchMovies(keyword, page)

    override fun insertMovie(movie: MovieEntity): Observable<Boolean> = dbHelper.insertMovie(movie)
    override fun deleteMovie(movie: MovieEntity): Observable<Boolean> = dbHelper.deleteMovie(movie)
    override fun getAllMovie(): Single<List<MovieEntity>> = dbHelper.getAllMovie()
    override fun fetchMoviesById(id: Int): Single<Boolean> = dbHelper.fetchMoviesById(id)

    override fun fetchMoviesByType(type: Int): Observable<List<MovieEntity>> =
        dbHelper.fetchMoviesByType(type)

    override fun fetchMoviesByTypeLiveData(type: Int): LiveData<List<MovieEntity>> =
        dbHelper.fetchMoviesByTypeLiveData(type)
}