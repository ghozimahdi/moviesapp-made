package com.blank.core.data.local.db

import androidx.lifecycle.LiveData
import androidx.sqlite.db.SimpleSQLiteQuery
import com.blank.core.utils.rx.observableIo
import com.blank.core.utils.rx.singleIo
import com.blank.models.local.entity.MovieEntity
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by GhoziMahdi on 09/12/2019.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */
@Singleton
class AppDbHelper @Inject constructor(private val appDatabase: AppDatabase) : DbHelper {
    override fun insertMovie(movie: MovieEntity): Observable<Boolean> =
        Observable.fromCallable {
            appDatabase.movieDao().insert(movie)
            true
        }.observableIo()

    override fun deleteMovie(movie: MovieEntity): Observable<Boolean> =
        Observable.fromCallable {
            appDatabase.movieDao().delete(movie)
            true
        }.observableIo()

    override fun getAllMovie(): Single<List<MovieEntity>> =
        appDatabase.movieDao().loadAll().singleIo()

    override fun fetchMoviesById(id: Int): Single<Boolean> =
        appDatabase.movieDao().findById(id).singleIo()
            .cast(MovieEntity::class.java)
            .map { true }

    override fun fetchMoviesByType(type: Int): Observable<List<MovieEntity>> =
        Observable.fromCallable {
            val query = SimpleSQLiteQuery("SELECT * FROM movies WHERE type = $type")
            appDatabase.movieDao().loadAll(query)
        }.observableIo()

    override fun fetchMoviesByTypeLiveData(type: Int): LiveData<List<MovieEntity>> {
        val query = SimpleSQLiteQuery("SELECT * FROM movies WHERE type = $type")
        return appDatabase.movieDao().loadAllLive(query)
    }
}