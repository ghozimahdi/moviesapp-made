package com.blank.core.data.local.db

import androidx.lifecycle.LiveData
import com.blank.models.local.entity.MovieEntity
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Created by GhoziMahdi on 09/12/2019.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */
interface DbHelper {
    fun insertMovie(movie: MovieEntity): Observable<Boolean>
    fun deleteMovie(movie: MovieEntity): Observable<Boolean>
    fun getAllMovie(): Single<List<MovieEntity>>
    fun fetchMoviesById(id: Int): Single<Boolean>
    fun fetchMoviesByType(type: Int): Observable<List<MovieEntity>>
    fun fetchMoviesByTypeLiveData(type: Int): LiveData<List<MovieEntity>>
}