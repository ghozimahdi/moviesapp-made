package com.blank.core.data.remote

import com.blank.core.utils.api.ResultState
import com.blank.core.utils.rx.flowableIo
import com.blank.core.utils.rx.observableIo
import com.blank.models.local.Genres
import com.blank.models.local.Movies
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by GhoziMahdi on 09/12/2019.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */
@Singleton
class AppApiHelper @Inject constructor(private val apiService: ApiService) : ApiHelper {
    override fun getPopularTVMovies(page: Int): Single<List<Movies>> =
        apiService.doGetMovies(endpointTvMoviesPopular(page))
            .map(Movies::mappingMoviesItems)
            .flowableIo()
            .mappingGenres(1)

    override fun getPopularTVMovies(): Single<ResultState> =
        getPopularTVMovies(1)
            .map(ResultState::Success)

    override fun getTopratedTVMovies(page: Int): Single<List<Movies>> =
        apiService.doGetMovies(endpointTvMoviesToprated(page))
            .map(Movies::mappingMoviesItems)
            .flowableIo()
            .mappingGenres(1)

    override fun getTopratedTVMovies(): Single<ResultState> =
        getTopratedTVMovies(1)
            .map(ResultState::Success)


    override fun getPopularMovies(page: Int): Single<List<Movies>> =
        apiService.doGetMovies(endpointMoviesPopular(page))
            .map(Movies::mappingMoviesItems)
            .flowableIo()
            .mappingGenres(0)

    override fun getPopularMovies(): Single<ResultState> =
        getPopularMovies(1)
            .map(ResultState::Success)

    override fun getTopRateMovies(page: Int): Single<List<Movies>> =
        apiService.doGetMovies(endpointMoviesToprated(page))
            .flowableIo()
            .map(Movies::mappingMoviesItems)
            .mappingGenres(0)

    override fun getTopRateMovies(): Single<ResultState> =
        getTopRateMovies(1)
            .map(ResultState::Success)

    override fun getUpComingMovies(page: Int): Single<List<Movies>> =
        apiService.doGetMovies(endpointMoviesUpcoming(page))
            .flowableIo()
            .map(Movies::mappingMoviesItems)
            .mappingGenres(0)

    override fun getUpComingMovies(): Single<ResultState> =
        getUpComingMovies(1)
            .map(ResultState::Success)

    override fun getGenres(type: Int, id: List<Int>): Single<MutableList<Genres>> =
        apiService.doGetGenresMovies(
            when (type) {
                0 -> endpointGenres()
                else -> endpointTvGenres()
            }
        )
            .observableIo()
            .map { Genres.mappingGenresItems(it.genres) }
            .flatMapIterable { it }
            .flatMap { genre ->
                Observable.fromIterable(id)
                    .filter {
                        genre.id == it
                    }.map {
                        genre
                    }
            }.toList()

    override fun searchMovies(keyword: String, page: Int): Single<List<Movies>> =
        apiService.doGetMovies(endpointSearchMovies(keyword, page))
            .map(Movies::mappingMoviesItems)
            .flowableIo()
            .mappingGenres(0)

    private fun Flowable<List<Movies>>.mappingGenres(type: Int) = flatMapIterable { it }
        .flatMap { resultItem ->
            getGenres(type, resultItem.genreIds)
                .map {
                    resultItem.genres = it
                    resultItem
                }.toFlowable()
        }.toList()
}