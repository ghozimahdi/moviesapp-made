package com.blank.core.data.remote

import com.blank.core.utils.api.ResultState
import com.blank.models.local.Genres
import com.blank.models.local.Movies
import io.reactivex.Single

/**
 * Created by GhoziMahdi on 09/12/2019.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */
interface ApiHelper {
    fun getPopularTVMovies(page: Int): Single<List<Movies>>
    fun getPopularTVMovies(): Single<ResultState>
    fun getTopratedTVMovies(page: Int): Single<List<Movies>>
    fun getTopratedTVMovies(): Single<ResultState>
    fun getPopularMovies(page: Int): Single<List<Movies>>
    fun getPopularMovies(): Single<ResultState>
    fun getTopRateMovies(page: Int): Single<List<Movies>>
    fun getTopRateMovies(): Single<ResultState>
    fun getUpComingMovies(page: Int): Single<List<Movies>>
    fun getUpComingMovies(): Single<ResultState>
    fun getGenres(type: Int, id: List<Int>): Single<MutableList<Genres>>
    fun searchMovies(keyword: String, page: Int): Single<List<Movies>>
}