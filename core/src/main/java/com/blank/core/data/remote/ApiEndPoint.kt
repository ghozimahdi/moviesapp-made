package com.blank.core.data.remote

import com.blank.core.utils.API_KEY

fun endpointTvMoviesPopular(page: Int) =
    "3/tv/popular?api_key=$API_KEY&language=en-US&page=$page"

fun endpointTvMoviesToprated(page: Int) =
    "3/tv/top_rated?api_key=$API_KEY&language=en-US&page=$page"

fun endpointMoviesPopular(page: Int) =
    "3/movie/popular?api_key=$API_KEY&language=en-US&page=$page"

fun endpointMoviesToprated(page: Int) =
    "3/movie/top_rated?api_key=$API_KEY&language=en-US&page=$page"

fun endpointMoviesUpcoming(page: Int) =
    "3/movie/upcoming?api_key=$API_KEY&language=en-US&page=$page"

fun endpointGenres() =
    "3/genre/movie/list?api_key=$API_KEY&language=en-US"

fun endpointTvGenres() =
    "3/genre/tv/list?api_key=$API_KEY&language=en-US"

fun endpointSearchMovies(keyword: String, page: Int) =
    "3/search/movie?api_key=$API_KEY&query=$keyword&language=en-US&page=$page&include_adult=false"

var ENDPOINT_IMAGE_CUSTOM = "https://image.tmdb.org/t/p/w500"
var CODE_POPULAR_MOVIES = 0
var CODE_TOP_RATED_MOVIES = 1
var CODE_UPCOMING_MOVIES = 2
var ERROR_CODE_MOVIES = "Code Request Movie Not Found"