package com.blank.core.utils.api

sealed class ResultState {
    data class Loading(val isLoading: Boolean) : ResultState()
    data class Success<T>(val data: T) : ResultState()
    data class Error(val e: Throwable) : ResultState()
}