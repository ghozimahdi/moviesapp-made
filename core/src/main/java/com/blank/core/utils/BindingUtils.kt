package com.blank.core.utils

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.blank.widget.utils.setImages

@BindingAdapter("imageUrl")
fun AppCompatImageView.loadImage(url: String?) {
    url?.let { setImages(it) }
}