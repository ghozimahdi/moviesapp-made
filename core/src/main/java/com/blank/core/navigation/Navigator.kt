package com.blank.core.navigation

import com.blank.models.local.Movies

interface Navigator {
    fun goToDetail(item: Movies)
    fun goToSearch()
}