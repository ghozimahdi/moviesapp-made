package com.blank.core.navigation

import android.content.Context
import android.content.Intent
import com.blank.core.utils.KEY_ITEM_MOVIES
import com.blank.models.local.Movies
import com.gaelmarhic.quadrant.QuadrantConstants
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NavigatorProvider @Inject constructor(@ApplicationContext private val context: Context) :
    Navigator {

    override fun goToDetail(item: Movies) {
        Intent().apply {
            setClassName(context, QuadrantConstants.DETAIL_MOVIE_ACTIVITY)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            putExtra(KEY_ITEM_MOVIES, item)
            context.startActivity(this)
        }
    }

    override fun goToSearch() {
        Intent().apply {
            setClassName(context, QuadrantConstants.SEARCH_ACTIVITY)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(this)
        }
    }
}