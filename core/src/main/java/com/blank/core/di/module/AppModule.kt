package com.blank.core.di.module

import com.blank.core.navigation.Navigator
import com.blank.core.navigation.NavigatorProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import org.greenrobot.eventbus.EventBus
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun provideEventBus(): EventBus = EventBus.getDefault()

    @Provides
    @Singleton
    fun provideNavigator(navigatorProvider: NavigatorProvider): Navigator = navigatorProvider
}