package com.blank.framework.di.dfm

import com.blank.core.data.DataManager
import com.blank.core.data.local.db.DbHelper
import com.blank.core.data.remote.ApiHelper
import com.blank.core.navigation.Navigator
import com.blank.core.utils.NetworkHelper
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import org.greenrobot.eventbus.EventBus

@EntryPoint
@InstallIn(ApplicationComponent::class)
interface CoreModuleDependencies {
    fun dataManager(): DataManager
    fun eventBus(): EventBus
    fun networkHelper(): NetworkHelper
    fun navigatorProvider(): Navigator
    fun apiHelper(): ApiHelper
    fun dbHelper(): DbHelper
}